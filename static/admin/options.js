jQuery(document).ready(function($){

     var new_row_html = $('.download-options tr:last').html()
     var new_atrow_html = $('.download-options tr:last').html()

     function new_row(){
         var c = $('.download-options tr').size()
         var html = $('<tr><td><input class="download-option-name" name="downloads['+c+'][name]" size="8" value="" type="text"></td><td><input class="download-option-format" name="downloads['+c+'][format]" value="" size="3" type="text"></td><td><input class="download-option-size" name="downloads['+c+'][size]" value="" size="3" type="text"></td><td><input class="download-option-url" name="downloads['+c+'][url]" value="" size="20" type="text"></td><td></td></tr>')
         $('.download-options tbody').append(html)
         add_actions(html)
     }

     function new_atrow(){
         var c = $('.attribution-options tr').size()
         var html = $('<tr><td><input class="attribution-option-html" name="attributions['+c+'][html]" size="8" value="" type="text"></td><td></td></tr>')
         $('.attribution-options tbody').append(html)
         add_actions_at(html)
     }

     function check_empty(){
         var need_new_row = false
         $('.download-options tr:last input:first').each(function(){
             if($(this).val() != '') need_new_row = true
         })
         if(need_new_row) new_row()				  

     }

     function check_empty_at(){
         var need_new_atrow = false
         $('.attribution-options tr:last input:first').each(function(){
             if($(this).val() != '') need_new_atrow = true
         })
		 if(need_new_atrow) new_atrow();
     }

     $('.download-options tr').each(function(){
         add_actions(this)
     })

     $('.attribution-options tr').each(function(){
         add_actions_at(this)
     })

     $('.attribution-options input').live('focus', function(){
         check_empty_at()
     })

     function add_actions(elm){
         var remove = $('<p class="download-option-remove">Remove</p>')
         $('td:last', elm).html(remove)
         remove.click(function(){
		     $(this).parent().parent().fadeOut(function(){$(this).remove()})
         })
     }

     function add_actions_at(elm){
         var remove = $('<p class="attribution-option-remove">Remove</p>')
         $('td:last', elm).html(remove)
         remove.click(function(){
		     $(this).parent().parent().fadeOut(function(){$(this).remove()})
         })
     }

})